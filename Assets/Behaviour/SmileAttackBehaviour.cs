﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmileAttackBehaviour : StateMachineBehaviour
{

    private Transform playerPos;
    private float speed = 2;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        playerPos = Player.instance.transform;
        animator.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, animator.GetComponent<Slime>().jumpForce));
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Vector2 target = new Vector2(playerPos.position.x, animator.transform.position.y);
        animator.transform.position = Vector2.MoveTowards(animator.transform.position, target, speed * Time.deltaTime);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
