﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailHideState : ISnailState
{
    private Snail snail;

    private float hideTimer;//tgian

    private float hideDuration;//tgian co dinh

    private void Hide()
    {
        snail.MyAnimator.SetFloat("speed", 0);
        snail.MyAnimator.SetTrigger("hide");

        if (!snail.seePlayer)
        {
            hideTimer += Time.deltaTime;
            if (hideTimer > hideDuration) // set animator is apear
            {
                snail.MyAnimator.SetTrigger("appear");
                if (hideTimer > hideDuration + 1.5) // change state is patrol
                {
                    snail.SnailChangeState(new SnailPatrolState());
                }
            }
        }
    }

    public void Execute()
    {
        Hide();
        //Debug.Log("snail hide");
    }

    public void Enter(Snail snail)
    {
        hideDuration = UnityEngine.Random.Range(2, 4);
        this.snail = snail;
        snail.seePlayer = true;
    }

    public void Exit()
    {
        snail.MyAnimator.ResetTrigger("hide");
        snail.MyAnimator.ResetTrigger("appear");
    }

    public void OntriggerEnter(Collider2D other)
    {
        if ((other.tag == "Knife") || (other.tag == "Sword"))
        {
            snail.SnailTakeDamageInHideState();
        }   
    }


}
