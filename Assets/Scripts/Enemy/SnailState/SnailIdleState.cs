﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailIdleState : ISnailState
{

    private Snail snail;

    private float idleTimer;//tgian

    private float idleDuration;//tgian co dinh

    private void Idle()
    {
        snail.MyAnimator.SetFloat("speed", 0);

        idleTimer += Time.deltaTime;

        if (idleTimer > idleDuration)
        {
            if (Random.Range(1, 3) == 1)
            {
                snail.ChangeDirection();
                snail.SnailChangeState(new SnailIdleState());
            }  
            snail.SnailChangeState(new SnailPatrolState());
        }
    }


    public void Execute()
    {
        Idle();
        //Debug.Log("snail Idle");
    }

    public void Enter(Snail snail)
    {
        idleDuration = UnityEngine.Random.Range(1, 7);
        this.snail = snail;
    }

    public void Exit()
    {

    }

    public void OntriggerEnter(Collider2D other)
    {
        
    }
}
