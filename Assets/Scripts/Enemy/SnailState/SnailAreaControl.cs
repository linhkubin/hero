﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailAreaControl : MonoBehaviour
{
    [SerializeField]
    private Snail snail;

    void Awake()
    {
        GameObject[] gameobject = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject character in gameobject)
        {
            Physics2D.IgnoreCollision(snail.GetComponent<Collider2D>(), character.GetComponent<Collider2D>(), true);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Edge" || other.tag == "JumpEdge")
        {
            snail.ChangeDirection();
        }
        //if (other.tag == "Enemy") // ignore different enemy
        //{
        //    //Debug.Log("Impact");
        //    Physics2D.IgnoreCollision(snail.GetComponent<Collider2D>(), other, true);
        //}
    }
}
