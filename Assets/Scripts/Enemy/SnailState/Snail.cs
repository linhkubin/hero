﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snail : Character
{
    private ISnailState currentState;

    public bool seePlayer;// hide when snail see Player 

    [SerializeField]
    private Canvas healthCanvas;// chong canvas xoay nguoc

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        SnailChangeState(new SnailIdleState());
        facingRight = false;
        //healthCanvas = transform.GetComponentInChildren<Canvas>();// lay canvas con
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsDead)
        {
            if (!TakingDamage)
            {
                currentState.Execute();
            }
        }
    }

    //awake ignore player
    void Awake()
    {
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), GameObject.Find("Player").GetComponent<Collider2D>(), true);

        GameObject[] gameobject = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject character in gameobject)
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), character.GetComponent<Collider2D>(), true);
        }
    }

    public void SnailChangeState(ISnailState newState)
    {
        if (currentState != null)
        {
            currentState.Exit();
        }

        currentState = newState;

        currentState.Enter(this);
    }

    public override void ChangeDirection()
    {
        Vector3 newCanvasT = healthCanvas.transform.position;
        healthCanvas.transform.SetParent(null);
        base.ChangeDirection();
        healthCanvas.transform.SetParent(this.transform);
        healthCanvas.transform.position = newCanvasT;

    }

    public Vector2 GetDirection()
    {
        return facingRight ? Vector2.right : Vector2.left;
    }

    public void Move() 
    {
        MyAnimator.SetFloat("speed", 1);

        transform.Translate(GetDirection() * (movementSpeed * Time.deltaTime));    
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        if (!(currentState is SnailHideState))
        {
            if (!IsDead)
            {
                if (other.tag == "Sword")
                {
                    StartCoroutine(TakeDamge(Player.instance.SwordDamage));
                }
                if (other.tag == "Knife")
                {
                    StartCoroutine(TakeDamge(Player.instance.KnifeDamage));
                }
            } 
  
        }

        base.OnTriggerEnter2D(other);
        currentState.OntriggerEnter(other);
    }

    public void SnailTakeDamageInHideState() 
    {
        StartCoroutine(TakeDamge(1));
    }

    public override bool IsDead
    {
        get { return health.CurrentVal <= 0; }
    }

    public override IEnumerator TakeDamge(int damage)
    {
        health.CurrentVal -= damage;

        if (!IsDead)
        {
            if (damage != 1)
            {
                MyAnimator.SetTrigger("damaged");
               
            }

        }
        else
        {
            Instantiate(GameManager.Instance.CoinPrefabs, new Vector3(transform.position.x, transform.position.y + 3), Quaternion.identity); // respawn coin

            MyAnimator.SetTrigger("die");

        }

        yield return null;
    }

    public override void Death()
    {
        GameManager.instance._updateProgressQuest(this.name);
        healthCanvas.enabled = false; //tat health bar
        Destroy(gameObject);
    }
}
