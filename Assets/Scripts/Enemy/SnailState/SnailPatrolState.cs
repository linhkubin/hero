﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailPatrolState : ISnailState
{
    private Snail snail;

    private float patrolTimer;//tgian

    private float patrolDuration;//tgian co dinh

    public void Patrol()
    {
        patrolTimer += Time.deltaTime;

        if (patrolTimer > patrolDuration)
        {
            snail.SnailChangeState(new SnailIdleState());
        }
    }

    public void Execute()
    {
        //Debug.Log("snail patrol");
        Patrol();
        snail.Move();
    }

    public void Enter(Snail snail)
    {
        patrolDuration = UnityEngine.Random.Range(3, 7);
        this.snail = snail;
    }

    public void Exit()
    {
        
    }

    public void OntriggerEnter(Collider2D other)
    {
        if (other.tag == "Edge" || other.tag == "JumpEdge")
        {
            snail.ChangeDirection();
        }
    }
}
