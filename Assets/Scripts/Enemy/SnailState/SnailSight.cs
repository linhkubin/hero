﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailSight : MonoBehaviour
{
    [SerializeField]
    private Snail snail;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player") // change hidestate
        {
            snail.SnailChangeState(new SnailHideState());
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            snail.seePlayer = false;
        }
    }
}
