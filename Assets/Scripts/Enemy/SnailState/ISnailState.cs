﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISnailState
{
    void Execute();
    void Enter(Snail snail);
    void Exit();
    void OntriggerEnter(Collider2D other);
}
