﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeIdleState : ISlimeState
{
    private Slime slime;

    private float idleTimer;//tgian

    private float idleDuration;//tgian co dinh

    private void Idle()
    {
        slime.MyAnimator.SetFloat("speed", 0);

        idleTimer += Time.deltaTime;

        if (idleTimer > idleDuration) // time's up and animator is not idle
        {
            if (Random.Range(1, 3) == 1)
            {
                slime.ChangeDirection();
                slime.SlimeChangeState(new SlimeIdleState());
            }

            slime.SlimeChangeState(new SlimePatrolState());
        }
    }

    public void Execute()
    {
        Idle();
        //Debug.Log("Slime idle");

        if (slime.Target != null)
        {
            slime.SlimeChangeState(new SlimePatrolState());
        }
    }

    public void Enter(Slime slime)
    {
        idleDuration = UnityEngine.Random.Range(1, 7);
        this.slime = slime;
    }

    public void Exit()
    {

    }

    public void OntriggerEnter(Collider2D other)
    {

    }
}
