﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeAttackState :ISlimeState
{
    private Slime slime;

    private void Attack()
    {
        slime.MyAnimator.SetTrigger("attack");

    }

    public void Execute()
    {
        Attack();

        //Debug.Log("slime attack");

        if (slime.Target == null)
        {
            slime.SlimeChangeState(new SlimePatrolState());
        }
        
    }

    public void Enter(Slime slime)
    {
        this.slime = slime;
    }

    public void Exit()
    {
        slime.MyAnimator.ResetTrigger("attack");
    }

    public void OntriggerEnter(Collider2D other)
    {

    }
}
