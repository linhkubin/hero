﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeSight : MonoBehaviour
{
    [SerializeField]
    private Slime slime;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
           slime.Target = other.gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            slime.Target = null;
        }
    }
}
