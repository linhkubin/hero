﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISlimeState
{
    void Execute();
    void Enter(Slime slime);
    void Exit();
    void OntriggerEnter(Collider2D other);
}
