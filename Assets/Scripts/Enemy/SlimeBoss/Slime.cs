﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : Character
{
    ISlimeState currenState;

    [SerializeField]
    public float jumpForce = 700;

    [SerializeField]
    private Canvas healthCanvas;// chong canvas xoay nguoc

    public GameObject Target { get; set; }

    [SerializeField]
    private EdgeCollider2D attackCollider;

    public EdgeCollider2D AttackCollider 
    {
        get { return attackCollider; } 
    }

    [SerializeField]
    private float attackRange;//

    public bool InAttackRange //
    {
        get
        {
            if (Target != null)
            {
                return Vector2.Distance(transform.position, Target.transform.position) <= attackRange;
            }
            return false;
        }
    }

    //--------------respawn Enemy------------

    [SerializeField]
    private Slime slimeSon;

    //---------------------------------------

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        SlimeChangeState(new SlimeIdleState());
        facingRight = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (!IsDead)
        {
            if (!TakingDamage)
            {
                currenState.Execute();
            }
            LookAtTarget();
        }
    }

    //awake ignore player
    void Awake()
    {
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), GameObject.Find("Player").GetComponent<Collider2D>(), true);

        GameObject[] gameobject = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject character in gameobject)
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), character.GetComponent<Collider2D>(), true);
        }
    }


    private void LookAtTarget()
    {
        if (Target != null && !(currenState is PigAttackState))
        {
            float xDirection = Target.transform.position.x - transform.position.x;

            if (xDirection < 0 && facingRight || xDirection > 0 && !facingRight)
            {
                //healthCanvas.transform.SetParent(null);// delete no khoi lop cha
                ChangeDirection();
                //healthCanvas.transform.SetParent(this.transform);
            }
        }

    }

    public override void ChangeDirection()
    {
        Vector3 newCanvasT = healthCanvas.transform.position;
        healthCanvas.transform.SetParent(null);
        base.ChangeDirection();
        healthCanvas.transform.SetParent(this.transform);
        healthCanvas.transform.position = newCanvasT;

    }


    public void RemoveTarget()
    {
        Target = null;
        SlimeChangeState(new SlimeIdleState());
    }

    public void SlimeChangeState(ISlimeState newState)
    {
        if (currenState != null)
        {
            currenState.Exit();
        }
        currenState = newState;
        currenState.Enter(this);
    }

    public Vector2 GetDirection()
    {
        return facingRight ? Vector2.right : Vector2.left;
    }

    public void Move()
    {
        if (Attack == false)
        {
            MyAnimator.SetFloat("speed", 1);
            transform.Translate(GetDirection() * (movementSpeed * Time.deltaTime));
        }
        //
    }

    public void SlimeAttack()
    {
        attackCollider.enabled = true;
    }

    public void SlimeEndAttack()
    {
        attackCollider.enabled = false;
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        if (!IsDead)
        {
            if (other.tag == "Sword")
            {
                StartCoroutine(TakeDamge(Player.instance.SwordDamage));
            }
            if (other.tag == "Knife")
            {
                StartCoroutine(TakeDamge(Player.instance.KnifeDamage));
            } 
        } 

        if (other.tag == "Edge")
        {
            this.ChangeDirection();
        }

        base.OnTriggerEnter2D(other);
        currenState.OntriggerEnter(other);
    }

    public override bool IsDead
    {
        get { return health.CurrentVal <= 0; }
    }

    public override IEnumerator TakeDamge(int damage)
    {
        health.CurrentVal -= damage;

        if (!IsDead)
        {
            MyAnimator.SetTrigger("damaged");
        }
        else
        {
            MyAnimator.SetTrigger("die");

            Instantiate(GameManager.Instance.CoinPrefabs, new Vector3(transform.position.x, transform.position.y + 3), Quaternion.identity); // respawn coin
        }
        yield return null;
    }

    public override void Death()
    {
        GameManager.instance._updateProgressQuest("Slime");

        healthCanvas.enabled = false; //tat health bar
        // respawn mini smile->
        
        if (slimeSon != null)
        {
            //Debug.Log("__________" + slimeSon.name);

            Instantiate(slimeSon, new Vector3(transform.position.x - Random.Range(0, 2), transform.position.y + Random.Range(0, 3)), Quaternion.identity);
            Instantiate(slimeSon, new Vector3(transform.position.x + Random.Range(0, 2), transform.position.y + Random.Range(0, 3)), Quaternion.identity);
        }
        
        Destroy(gameObject);
    }
}
