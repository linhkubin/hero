﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimePatrolState : ISlimeState
{
    private Slime slime;

    private float patrolTimer;//tgian

    private float patrolDuration;//tgian co dinh

    public void Patrol()
    {
        slime.Move();
        patrolTimer += Time.deltaTime;
        if (patrolTimer > patrolDuration) // time's up and pig is grounded
        {
            if (slime.GetComponent<Rigidbody2D>().velocity.y == 0 )
            {
                slime.SlimeChangeState(new SlimeIdleState());
            }
        }
    }

    public void Execute()
    {
        //Debug.Log("slime patrol");
        Patrol();

        if (slime.Target != null && slime.InAttackRange)
        {
            slime.SlimeChangeState(new SlimeAttackState());
        }

    }

    public void Enter(Slime slime)
    {
        patrolDuration = UnityEngine.Random.Range(3, 7);
        this.slime = slime;
    }

    public void Exit()
    {
        slime.MyAnimator.SetFloat("speed", 0);
    }

    public void OntriggerEnter(Collider2D other)
    {
        if (other.tag == "Knife")
        {
            slime.Target = Player.Instance.gameObject;
        }

        if (other.tag == "JumpEdge")
        {
            slime.GetComponent<Rigidbody2D>().AddForce(new Vector2(0,500));
        }
    }
}
