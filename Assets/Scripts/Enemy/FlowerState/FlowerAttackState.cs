﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerAttackState : IFlowerState
{
    private Flower flower;

    private void Attack()
    {
        if (!flower.MyAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Damaged"))
        {
            
            flower.MyAnimator.SetTrigger("attack");

        }
        if (flower.Target == null) // time's up and animator is not idle
        {
            flower.FlowerChangeState(new FlowerIdleState());
        }
    }

    public void Execute()
    {
        Attack();
        //Debug.Log("flower attack");
    }

    public void Enter(Flower flower)
    {
        this.flower = flower;
    }

    public void Exit()
    {

    }

    public void OntriggerEnter(Collider2D other)
    {
        
    }
}
