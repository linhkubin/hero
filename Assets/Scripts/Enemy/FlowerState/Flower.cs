﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flower : Character
{
    private IFlowerState currenState;

    public GameObject Target { get; set; }

    [SerializeField]
    private Canvas healthCanvas;

    [SerializeField]
    private float attackRange;//

    public bool InAttackRange //
    {
        get
        {
            if (Target != null)
            {
                return Vector2.Distance(transform.position, Target.transform.position) <= attackRange;
            }
            return false;
        }
    }

    [SerializeField]
    private BoxCollider2D attackCollider;

    public BoxCollider2D AttackCollider
    {
        get { return attackCollider; }
    }


    void Awake()
    {
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), GameObject.Find("Player").GetComponent<Collider2D>(), true);

        GameObject[] gameobject = GameObject.FindGameObjectsWithTag("Enemy");
        foreach(GameObject character in gameobject)
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), character.GetComponent<Collider2D>() , true);
        }
    
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        FlowerChangeState(new FlowerIdleState());
        facingRight = false;
    }

    public void FlowerChangeState(IFlowerState newState)
    {
        if (currenState != null)
        {
            currenState.Exit();
        }
        currenState = newState;
        currenState.Enter(this);
    }
    // Update is called once per frame
    void Update()
    {
        if (!IsDead)
        {
            if (!TakingDamage)
            {
                currenState.Execute();
            }
            LookAtTarget();
        }
    }

    private void LookAtTarget()
    {
        if (Target != null )
        {
            float xDirection = Target.transform.position.x - transform.position.x;

            if (xDirection < 0 && facingRight || xDirection > 0 && !facingRight)
            {
                ChangeDirection();
            }
        }
    }

    public void RemoveTarget()
    {
        Target = null;
        FlowerChangeState(new FlowerIdleState());
    }

    public override void ChangeDirection()
    {
        if (!Attack)
        {
            Vector3 newCanvasT = healthCanvas.transform.position;
            healthCanvas.transform.SetParent(null);
            base.ChangeDirection();
            healthCanvas.transform.SetParent(this.transform);
            healthCanvas.transform.position = newCanvasT;
        }

    }

    public void FlowerAttack()
    {
        AttackCollider.enabled = true;
    }

    public void EndFlowerAttack()
    {
        AttackCollider.enabled = false;
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {

        if (!IsDead)
        {
            if (other.tag == "Sword")
            {
                StartCoroutine(TakeDamge(Player.instance.SwordDamage));
            }
            if (other.tag == "Knife")
            {
                StartCoroutine(TakeDamge(Player.instance.KnifeDamage));
            }
        } 


        base.OnTriggerEnter2D(other);
        currenState.OntriggerEnter(other);
    }

    public Vector2 GetDirection()
    {
        return facingRight ? Vector2.right : Vector2.left;
    }

    public override bool IsDead
    {
        get { return health.CurrentVal <= 0; }
    }

    public override IEnumerator TakeDamge(int damage)
    {
        health.CurrentVal -= damage;

        if (!IsDead)
        {
            MyAnimator.SetTrigger("damaged");
        }
        else
        {
            Instantiate(GameManager.Instance.CoinPrefabs, new Vector3(transform.position.x, transform.position.y + 3), Quaternion.identity); // respawn coin

            MyAnimator.SetTrigger("die");
        }

        yield return null;
    }

    public override void Death()
    {
        GameManager.instance._updateProgressQuest(this.name);
        healthCanvas.enabled = false; //tat health bar
        Destroy(gameObject);
    }
}
