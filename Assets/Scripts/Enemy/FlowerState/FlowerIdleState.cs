﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerIdleState : IFlowerState
{
    private Flower flower;

    private void Idle()
    {
        if (flower.Target != null) // time's up and animator is not idle
        {
            flower.FlowerChangeState(new FlowerAttackState());
        }
    }

    private void FlowerResetTrigger()
    {
        flower.MyAnimator.ResetTrigger("attack");
        flower.MyAnimator.ResetTrigger("damaged");
    }

    public void Execute()
    {
        Idle();
        //Debug.Log("flower idle");
    }

    public void Enter(Flower flower)
    {
        this.flower = flower;
        FlowerResetTrigger();
    }

    public void Exit()
    {
        
    }

    public void OntriggerEnter(Collider2D other)
    {
       
    }
}
