﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerSight : MonoBehaviour
{
    [SerializeField]
    private Flower flower;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            flower.Target = other.gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            flower.Target = null;
        }
    }
}
