﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFlowerState
{
    void Execute();
    void Enter(Flower flower);
    void Exit();
    void OntriggerEnter(Collider2D other);
}
