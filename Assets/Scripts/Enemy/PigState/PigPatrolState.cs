﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigPatrolState : IPigState
{
    private Pig pig;

    private float patrolTimer;//tgian

    private float patrolDuration;//tgian co dinh

    public void Patrol()
    {
        pig.Move();
        patrolTimer += Time.deltaTime;
        if (patrolTimer > patrolDuration && (pig.GetComponent<Rigidbody2D>().velocity.y == 0) ) // time's up and pig is grounded
        {
            pig.PigChangeState(new PigIdleState());
        }
    }

    public void Execute()
    {
        //Debug.Log("pig patrol");
        Patrol();

        if (pig.Target != null && pig.InAttackRange)
        {
            pig.PigChangeState(new PigAttackState());
        }
        
    }

    public void Enter(Pig pig)
    {
        patrolDuration = UnityEngine.Random.Range(3, 7);
        this.pig = pig;
    }

    public void Exit()
    {

    }

    public void OntriggerEnter(Collider2D other)
    {
        if (other.tag == "Knife")
        {
            pig.Target = Player.Instance.gameObject;
        }
    }
}
