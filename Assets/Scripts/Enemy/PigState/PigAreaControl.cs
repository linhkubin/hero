﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigAreaControl : MonoBehaviour
{
    [SerializeField]
    private Pig pig;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "JumpEdge")
        {
            pig.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, pig.JumpForce));//add jump for pig
            pig.MyAnimator.SetTrigger("jump");
        }

        if (other.tag == "JumpOrNot") // jump if interac with box jumpornot
        {

            if (Random.Range(1, 3) > 1) // random 1-2
            {
                if ( ( (other.transform.position.x - pig.transform.position.x) > 0 && (other.name == "onTheLeft") )
                    ||( (other.transform.position.x - pig.transform.position.x) < 0 && (other.name == "onTheRight") ))
                {
                    pig.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, pig.JumpForce));//add jump for pig
                    pig.MyAnimator.SetTrigger("jump");
                }
            }
            
        }

        if (other.tag == "Wall")
        {
            pig.ChangeDirection();
        }
    }
}
