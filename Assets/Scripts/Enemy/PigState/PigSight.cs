﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigSight : MonoBehaviour
{
    [SerializeField]
    private Pig pig;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            pig.Target = other.gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            pig.Target = null;
        }
    }


}
