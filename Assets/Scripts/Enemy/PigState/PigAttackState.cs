﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigAttackState : IPigState
{
    private Pig pig;

    public void Execute()
    {
        Attack();

        if (pig.Target == null)
        {  
            // check pig toward Player
            if (!pig.PigCheckFacing())
            {
                pig.ChangeDirection();
            }

            if (pig.Target == null)
            {
                pig.PigChangeState(new PigPatrolState());
            }
        }
    }

    private void Attack()
    {
        if (!pig.MyAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Damaged"))
        {
            pig.Move();
        } 

        //Debug.Log("pig attack");
    }

    public void Enter(Pig pig)
    {
        this.pig = pig;
    }

    public void Exit()
    {

    }

    public void OntriggerEnter(Collider2D other)
    {

    }

}
