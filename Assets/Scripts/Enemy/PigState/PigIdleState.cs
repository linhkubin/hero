﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigIdleState : IPigState
{
    private Pig pig;

    private float idleTimer;//tgian

    private float idleDuration;//tgian co dinh

    private void Idle()
    {
        pig.MyAnimator.SetFloat("speed", 0);

        idleTimer += Time.deltaTime;

        if (idleTimer > idleDuration && !pig.MyAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Idle")) // time's up and animator is not idle
        {
            if (Random.Range(1, 3) == 1)
	        {
                pig.ChangeDirection();
                pig.PigChangeState(new PigIdleState());
	        }      
            pig.PigChangeState(new PigPatrolState());
        }
    }

    public void Execute()
    {
        Idle();
        //Debug.Log("pig idle");

        if (pig.Target != null)
        {
            pig.PigChangeState(new PigPatrolState());
        }
    }

    public void Enter(Pig pig)
    {
        idleDuration = UnityEngine.Random.Range(1, 7);
        this.pig = pig;
    }

    public void Exit()
    {

    }

    public void OntriggerEnter(Collider2D other)
    {
        if (other.tag == "Knife")
        {
            pig.Target = Player.Instance.gameObject;
        }
    }
}
