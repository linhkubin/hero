﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pig : Character
{
    IPigState currenState;

    [SerializeField]
    public float JumpForce = 550;

    public GameObject Target { get; set; }

    [SerializeField]
    private Canvas healthCanvas;// chong canvas xoay nguoc

    [SerializeField]
    private float attackRange;//

    [SerializeField]
    private GameObject attackCollider;

    //public int AttackDamage
    //{
    //    get
    //    {
    //        return 20;
    //    }
    //}

    public bool InAttackRange //
    {
        get 
        {
            if (Target != null)
            {
                return Vector2.Distance(transform.position, Target.transform.position) <= attackRange;
            }
            return false;
        }
    }


    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        PigChangeState(new PigIdleState());
        facingRight = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (!IsDead)
        {
            if (!TakingDamage)
            {
                currenState.Execute();
            }
            LookAtTarget();
        }
    }

    private void LookAtTarget()
    {
        if (Target != null && !(currenState is PigAttackState))
        {
            float xDirection = Target.transform.position.x - transform.position.x;

            if (xDirection < 0 && facingRight || xDirection > 0 && !facingRight)
            {
                //healthCanvas.transform.SetParent(null);// delete no khoi lop cha
                ChangeDirection();
                //healthCanvas.transform.SetParent(this.transform);
            }
        }
       
    }

    public bool PigCheckFacing() // if Pig toward Player is true, different direction is false
    {

        float xDirection = GameObject.Find("Player").transform.position.x - transform.position.x;

        if (xDirection < 0 && facingRight || xDirection > 0 && !facingRight)
        {
            return false;
        }

        return true;


    }

    public void RemoveTarget()
    {
        Target = null;
        PigChangeState(new PigIdleState());
    }

    //awake ignore player
    void Awake()
    {
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), GameObject.Find("Player").GetComponent<Collider2D>(), true);

        GameObject[] gameobject = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject character in gameobject)
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), character.GetComponent<Collider2D>(), true);
        }
    }

    public void PigChangeState(IPigState newState)
    {
        if (currenState != null)
        {
            currenState.Exit();
        }
        currenState = newState;
        currenState.Enter(this);
    }

    public override void ChangeDirection()
    {
        Vector3 newCanvasT = healthCanvas.transform.position;
        healthCanvas.transform.SetParent(null);
        base.ChangeDirection();
        healthCanvas.transform.SetParent(this.transform);
        healthCanvas.transform.position = newCanvasT;

    }

    public Vector2 GetDirection()
    {
        return facingRight ? Vector2.right : Vector2.left;
    }

    public void Move()
    {
        if (Attack == false)
        {
            MyAnimator.SetFloat("speed", 1);
            transform.Translate(GetDirection() * (movementSpeed * Time.deltaTime));
        }
        //
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        if (!IsDead)
        {

            if (other.tag == "Sword")
            {
                StartCoroutine(TakeDamge(Player.instance.SwordDamage));
            }
            if (other.tag == "Knife")
            {
                Target = other.gameObject;
                StartCoroutine(TakeDamge(Player.instance.KnifeDamage));
            }   
        }   

        base.OnTriggerEnter2D(other);
        currenState.OntriggerEnter(other);
    }

    public override bool IsDead
    {
        get { return health.CurrentVal <= 0; }
    }

    public override IEnumerator TakeDamge(int damage)
    {
        health.CurrentVal -= damage;

        if (!IsDead)
        {
            MyAnimator.SetTrigger("damaged");
        }
        else
        {
            attackCollider.SetActive(false);
            Instantiate(GameManager.Instance.CoinPrefabs, new Vector3(transform.position.x, transform.position.y + 3), Quaternion.identity); // respawn coin

            MyAnimator.SetTrigger("die");

        }
        yield return null;

    }

    public override void Death()
    {
        GameManager.instance._updateProgressQuest(this.name);
        healthCanvas.enabled = false; //tat health bar
        Destroy(gameObject);
    }
}
