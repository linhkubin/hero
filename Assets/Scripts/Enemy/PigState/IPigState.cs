﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPigState 
{
    void Execute();
    void Enter(Pig pig);
    void Exit();
    void OntriggerEnter(Collider2D other);
}
