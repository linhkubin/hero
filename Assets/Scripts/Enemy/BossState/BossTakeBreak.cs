﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTakeBreak : IBossState
{

    private float takeTimer;

    private float takeTimeDuration = 2.5f;

    private Boss boss;

    private void BossTakeBreakTime()
    {
        boss.MyAnimator.SetTrigger("tired");

        takeTimer += Time.deltaTime;

        if (takeTimer > takeTimeDuration) // time's up and animator is not idle
        {
            boss.MyAnimator.SetTrigger("idle");
            boss.BossChangeState(new BossIdleState());
        }
    }
    
    public void Execute()
    {
        Debug.Log("boss tired");
        BossTakeBreakTime();    
    }

    public void Enter(Boss boss)
    {
        this.boss = boss;
    }

    public void Exit()
    {
        boss.MyAnimator.ResetTrigger("tired");
        
    }

    public void OntriggerEnter(Collider2D other)
    {
        
    }
}
