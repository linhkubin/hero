﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttackState : IBossState 
{
    private Boss boss;

    public void Execute()
    {
        //Debug.Log("boss attack");

        Attack();

        //Debug.Log("slime attack");

        if (boss.Target == null)
        {
            boss.BossChangeState(new BossIdleState());
        }
    }

    private void Attack()
    {
        if (!boss.BossAttack)
        {
            if (boss.lv2)
            {
               
                switch (Random.Range(0,3))
                {
                    case 0: 
                        {
                            boss.MyAnimator.SetTrigger("skill0");
                            break;
                        }

                    case 1:
                        {
                            boss.MyAnimator.SetTrigger("skill1");
                            break;
                        }

                    case 2:
                        {
                            boss.MyAnimator.SetTrigger("skill2");
                            break;
                        }

                    default:
                        break;
                }
            }
            else
            {
                boss.MyAnimator.SetTrigger("skill2");
            }
        }
    }

    public void Enter(Boss boss)
    {
        this.boss = boss;
    }

    public void Exit()
    {
        boss.MyAnimator.ResetTrigger("skill0");
        boss.MyAnimator.ResetTrigger("skill1");
        boss.MyAnimator.ResetTrigger("skill2");
    }

    public void OntriggerEnter(Collider2D other)
    {

    }
}
