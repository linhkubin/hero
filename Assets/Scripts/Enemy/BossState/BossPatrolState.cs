﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPatrolState : IBossState 
{
    private Boss boss;

    private float patrolTimer;//tgian

    private float patrolDuration;//tgian co dinh

    public void Patrol()
    {
        boss.Move();
        patrolTimer += Time.deltaTime;
        if (patrolTimer > patrolDuration) // time's up and pig is grounded
        {
            boss.BossChangeState(new BossIdleState());
        }
    }

    public void Execute()
    {
        //Debug.Log("boss patrol");
        Patrol();

        if (boss.Target != null && boss.InAttackRange)
        {
            boss.BossChangeState(new BossAttackState());
        }

    }

    public void Enter(Boss boss)
    {
        patrolDuration = UnityEngine.Random.Range(3, 7);
        this.boss = boss;
    }

    public void Exit()
    {
        boss.MyAnimator.SetFloat("speed",0);
    }

    public void OntriggerEnter(Collider2D other)
    {
        
    }
}
