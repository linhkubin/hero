﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicSphere : MonoBehaviour
{
    //[SerializeField]
    //private int damage = 30;

    //private Rigidbody2D myRigidbody;

    [SerializeField]
    private float speed= 3;

    //public GameObject effect;

    public Transform MyTarget { get; set; }

    private void Start()
    {
        
        //myRigidbody = GetComponent<Rigidbody2D>();
        
    }

    private void FixedUpdate()
    {
        transform.Translate(Vector2.up * speed * Time.deltaTime);

        //if (MyTarget != null)
        //{
            //Vector2 direction = MyTarget.position - transform.position;

            //myRigidbody.velocity = direction.normalized * speed;

        //}
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //GetComponent<Animator>().SetTrigger("impact");

            //StartCoroutine(Player.instance.TakeDamge(damage));

            ////myRigidbody.velocity = Vector2.zero;

            //MyTarget = null;

            //StartCoroutine(Player.instance.TakeDamge(damage));

        }

        if (other.tag == "Edge")
        {
            //Instantiate(effect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

    }
}
