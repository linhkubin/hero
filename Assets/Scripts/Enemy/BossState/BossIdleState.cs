﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossIdleState : IBossState
{
    private Boss boss;

    private float idleTimer;//tgian

    private float idleDuration;//tgian co dinh

    public void Execute()
    {
        Idle();

        //Debug.Log("boss idle");

        if (boss.Target != null)
        {
            boss.BossChangeState(new BossPatrolState());
        }
    }

    private void Idle()
    {
        boss.MyAnimator.SetFloat("speed", 0);

        idleTimer += Time.deltaTime;

        if (idleTimer > idleDuration) // time's up and animator is not idle
        {
            if (Random.Range(1, 3) == 1)
            {
                boss.ChangeDirection();
                boss.BossChangeState(new BossIdleState());
            }
            boss.BossChangeState(new BossPatrolState());
        }
    }

    public void Enter(Boss boss)
    {
        idleDuration = UnityEngine.Random.Range(1, 7);
        this.boss = boss;
    }

    public void Exit()
    {

    }

    public void OntriggerEnter(Collider2D other)
    {

    }
}
