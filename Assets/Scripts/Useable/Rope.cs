﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour, IUseable
{
    [SerializeField]
    private Collider2D platformCollider;

    public void Use()
    {
        if (Player.Instance.OnLadder)
        {
            // stop climbing
            UseLadder(false, 2, 0, 1);
            // unignore platform
            Physics2D.IgnoreCollision(Player.Instance.GetComponent<Collider2D>(), platformCollider, false);
        }
        else
        {
            // start climbing
            UseLadder(true, 0, 1, 0);
            // ignore platform
            Physics2D.IgnoreCollision(Player.Instance.GetComponent<Collider2D>(), platformCollider, true);
            // set landing = true
            Player.Instance.MyAnimator.SetBool("land", true);
            // set transform player middle rope
            Vector3 temp = Player.Instance.transform.position;
            temp.x = this.transform.position.x;
            Player.Instance.transform.position = temp; 
        }
    }

    private void UseLadder(bool onLadder, int gravity, int layerWeight, int animSpeed)
    {
        Player.Instance.OnLadder = onLadder; // set on ladder
        Player.Instance.MyRigidbody.gravityScale = gravity; // set luc hut
        Player.Instance.MyAnimator.SetLayerWeight(2, layerWeight);// set layerWeight
        Player.Instance.MyAnimator.speed = animSpeed; // set speed animation
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            UseLadder(false, 2, 0, 1);
            // un ignore platform
            Physics2D.IgnoreCollision(Player.Instance.GetComponent<Collider2D>(), platformCollider, false);
        }
    }
}
