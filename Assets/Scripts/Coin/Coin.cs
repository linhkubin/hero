﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    void Awake()
    {
        GameObject[] gameobject = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject character in gameobject)
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), character.GetComponent<Collider2D>(), true);
        }

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
