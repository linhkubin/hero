﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

[Serializable]
public class Quest 
{

    //[SerializeField]
    //private Image icon;

    [SerializeField]
    private int target;

    [SerializeField]
    private string name;

    [SerializeField]
    private int progress;

    //public Image Icon { 
    //    get
    //    {
    //        return icon;
    //    }
    //    set
    //    {
    //        this.icon = value;
    //    }
    //}

    public int Target
    {
        get
        {
            return target;
        }
        set
        {
            this.target = value;
        }
    }

    public string Name
    {
        get
        {
            return name;
        }
        set
        {
            this.name = value;
        }
    }

    public int Progress
    {
        get
        {
            return progress;
        }
        set
        {
            this.progress = value;
        }
    }

    public void Initialize()
    {
        //this.Icon = icon;
        this.Target = target;
        this.Name = name;
        this.Progress = progress;
    }
}
