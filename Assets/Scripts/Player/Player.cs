﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character {


    //public event DeadEventHandler Dead;

    public  static Player instance;

    public static Player Instance
    {
        get 
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<Player>();
            }
            return Player.instance; 
        }
    }
    //------------------------------------------------------------

    public Rigidbody2D MyRigidbody { get; set;}

    [SerializeField]
    private float jumpForce;

    public bool Jump { get; set; }

    public bool OnGround { get; set; }

    //----------------------------------------------------------------------
    // check grounded
    [SerializeField]
    private Transform[] groundPoints;

    [SerializeField]
    private LayerMask whatIsGround;

    [SerializeField]
    private float groundRadius;
    //-----------------------------------------------------------------------
    // use rope
    private IUseable useable; 

    public bool OnLadder { get; set; }

    [SerializeField]
    private float climbSpeed;
    //-------------------------------------------------------------------------
    //attack time AD
    private float adAttackTime = 3;

    private float attackTimer;
    //-------------------------------------------------------------------------

    //knife
    public int KnifeDamage { get; set; }

    [SerializeField]
    private GameObject knifePrefab;

    [SerializeField]
    private Transform knifePosition;
    //-------------------------------------------------------------------------

    // sword collider
    public int SwordDamage { get; set; }

    [SerializeField]
    private EdgeCollider2D swordCollider;

    public EdgeCollider2D SwordCollider
    {
        get { return swordCollider; }
    }
    //-------------------------------------------------------------------------

    // immortal
    private bool immortal = false;

    [SerializeField]
    private float immortalTime = 1;

    private SpriteRenderer spriteRenderer;
    //-------------------------------------------------------------------------
    //--------------save position--------------------------------------------
    private Vector3 savePosition;

    //------------------------------------------------------------------------


	// Use this for initialization
	public override void Start () {
		base.Start();

        savePosition = transform.position;

        MyRigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        OnLadder = false;

        SwordDamage = 10;
        KnifeDamage = 8;

	}
	
	// Update is called once per frame
	void Update () {
        if (!TakingDamage && !IsDead)
        {
            HandleInput();
        }
	}

    void FixedUpdate()
    {
        if (!TakingDamage && !IsDead)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            HandleMovement(horizontal, vertical); // movement
            Flip(horizontal); // flip direction
            HandleLayerWeight(); // layer
            OnGround = IsGrounded(); // check ground
            timeAtack(); // check time ad attack

            //Debug.Log("IsGrounded() : " + IsGrounded() + "; OnGround2D : " + OnGround);
        }
    }

    // flip direction
    private void Flip(float horizontal)
    {
        if ( ((horizontal < 0 && facingRight) || (horizontal > 0 && !facingRight)) && !Attack) //&& !this.MyAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            ChangeDirection();
        }
    }

    // move handle
    private void HandleMovement(float horizontal, float vertical) 
    {
        

        if (MyRigidbody.velocity.y < 0)
        {
            MyAnimator.SetBool("land", true);
        }

        if (!Attack)
        {
            MyRigidbody.velocity = new Vector2(horizontal * movementSpeed, MyRigidbody.velocity.y);
            MyAnimator.SetFloat("speed", Mathf.Abs(horizontal));

            if (Jump && MyRigidbody.velocity.y == 0)
            {
                MyRigidbody.AddForce(new Vector2(0, jumpForce));
            }
        }

        if (OnLadder)
        {
            MyAnimator.speed = Mathf.Abs(vertical); // set speed.vertical co the - or +
            MyRigidbody.velocity = new Vector2(0, vertical * climbSpeed);
        }
    }

    // handle Input
    private void HandleInput() 
    {
        if (!OnLadder && !Attack)
        {
            //jump
            if (Input.GetKeyDown(KeyCode.Space))
            {
                MyAnimator.SetTrigger("jump");

            }
            // attack
            if (Input.GetKeyDown(KeyCode.C))
            {
                attackTimer = 0;
                adAttack();
                MyAnimator.SetTrigger("attack");
            }
            // use rope
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                Use();
            }
            // throw
            if (Input.GetKeyDown(KeyCode.V))
            {
                MyAnimator.SetTrigger("throw");
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Use();
            }
        }
        
            
    }

    //throw
    public void ThrowKnife(int value)
    {

        if (!OnGround && value == 1 || OnGround && value == 0)
        {
            //Debug.Log("valueThrow" + value);
            // 
            if (facingRight)
            {
                GameObject tmp = (GameObject)Instantiate(knifePrefab, knifePosition.position, Quaternion.Euler(new Vector3(0, 0, -90)));
                tmp.GetComponent<Knife>().Initialize(Vector2.right);
            }
            else
            {
                GameObject tmp = (GameObject)Instantiate(knifePrefab, knifePosition.position, Quaternion.Euler(new Vector3(0, 0, 90)));
                tmp.GetComponent<Knife>().Initialize(Vector2.left);
            }
        }
    }

    public void MeleeAttack()
    {
        SwordCollider.enabled = true;
    }

    //ad attack;
    private void adAttack()
    {
        if (OnGround)
        {
            int tempAdAttack = MyAnimator.GetInteger("adAttack");
            tempAdAttack = tempAdAttack > 1 ? 0 : tempAdAttack + 1;
            MyAnimator.SetInteger("adAttack", tempAdAttack);
        }
    }
    private void timeAtack()
    {
        attackTimer += Time.deltaTime;
        if (attackTimer >= adAttackTime || Jump) // time up attack or jump reset ad attack
        {
            MyAnimator.SetInteger("adAttack", -1);
        }
    }

    //recovery
    public void Recovery(int heal)
    {
        this.health.CurrentVal += heal;
    }

    //--------------

    //check grounded
    private bool IsGrounded()
    {
        //Debug.Log("velocity.y : " + MyRigidbody.velocity.y);
        if (MyRigidbody.velocity.y <= 0)
        {
            foreach (Transform point in groundPoints)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);

                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    //handle layer weight
    private void HandleLayerWeight()
    {
        if (!OnGround)
        {
            MyAnimator.SetLayerWeight(1, 1);
        }
        else
        {
            MyAnimator.SetLayerWeight(1, 0);
        }
    }
    //-------------------------------------------------------------------------
    //  use rope
    private void Use() 
    {
        if (useable != null)
        {
            useable.Use();
        }
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Useable")
        {
            useable = other.GetComponent<IUseable>();
        }
        if (other.tag == "EnemyAttackCollider")
        {
            //Debug.Log("take damge");
            StartCoroutine(TakeDamge(10));

        }

        if (other.tag == "BossSpell")
        {
            StartCoroutine(TakeDamge(30));
        }

        if (other.tag == "SavePosition")
        {
            savePosition = other.transform.position;
        }

        base.OnTriggerEnter2D(other);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Useable")
        {
            useable = null;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Coin")
        {
            GameManager.Instance.CollectedCoins++;
            Destroy(other.gameObject);
        }
    }
    //---------------------------------------------------------------------------

    public override bool IsDead
    {
        get { return health.CurrentVal <= 0; }
    }

    //public void OnDead()
    //{
    //    if (Dead != null)
    //    {
    //        Dead();
    //    }
    //}

    // nhap nhay
    private IEnumerator IndicateImmortal()
    {
        while (immortal)
        {
            spriteRenderer.enabled = false;
            yield return new WaitForSeconds(0.1f);
            spriteRenderer.enabled = true;
            yield return new WaitForSeconds(0.1f);

        }
        yield return null;
    }

    public override IEnumerator TakeDamge(int damage)
    {
        if (!immortal && !IsDead)
        {
            health.CurrentVal -= damage;
            //CombatTextManager.Instance.CreateText(transform.position, "10", Color.red, false);
            //Debug.Log("player " + health.CurrentVal);
            if (!IsDead)
            {
                MyAnimator.SetTrigger("damaged");

                immortal = true;

                StartCoroutine(IndicateImmortal());
                yield return new WaitForSeconds(immortalTime);

                immortal = false;
            }
            else
            {
                //OnDead();
                MyAnimator.SetLayerWeight(1, 0);

                MyAnimator.SetTrigger("die");
            }
        }

        yield return null;
    }

    public override void Death()
    {
        MyAnimator.ResetTrigger("jump");
        MyRigidbody.velocity = Vector2.zero;
        MyAnimator.SetTrigger("idle");
        health.CurrentVal = health.MaxVal / 2;
        transform.position = savePosition;
        GetComponent<BoxCollider2D>().enabled = true;
    }
    
}
