﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField]
    private GameObject coinPrefabs;

    [SerializeField]
    private Text coinText;

    private int collectedCoins;

    //-----------------quest----------------------
    [SerializeField]
    protected Quest[] quest;

    [SerializeField]
    private Sprite completeQuest;

    [SerializeField]
    private Image[] iconQuest;

    [SerializeField]
    private Text[] textQuest;

    //--------------------------------------------

    //----------------boss------------------------
    [SerializeField]
    private GameObject healthBoss;

    [SerializeField]
    private GameObject boss;

    private bool init = false;
    //--------------------------------------------

    void Update()
    {
        updateQuest();
        initBoss();
    }

    private void updateQuest()
    {
        for (int i = 0; i < quest.Length ; i++)
        {
            if (quest[i].Progress == quest[i].Target)
            {
                iconQuest[i].sprite = completeQuest;
            }
            
            textQuest[i].text = " " + quest[i].Name + " : " + quest[i].Progress + " / " + quest[i].Target + ".";
        }
    }

    private void initBoss()
    {
        if (!init)
        {
            for (int i = 0; i < quest.Length; i++)
            {
                if (quest[i].Progress < quest[i].Target)
                {
                    return;
                }
            }

            healthBoss.SetActive(true);
            boss.SetActive(true);

            init = true;
        }
        
    }

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }
            return GameManager.instance;
        }

    }

    public GameObject CoinPrefabs
    {
        get
        {
            return coinPrefabs;
        }
    }

    public int CollectedCoins
    {
        get
        {
            return collectedCoins;
        }
        set
        {
            coinText.text = value.ToString();
            collectedCoins = value;
        }
    }


    public void _updateProgressQuest(string character)
    {
        for (int i = 0; i < quest.Length; i++)
        {
            if (quest[i].Name == character)
            {
                quest[i].Progress++;
            } 
        }
    }
}
