﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{

    [SerializeField]
    protected float movementSpeed;

    public bool facingRight;

    [SerializeField]
    protected Stat health;

    //[SerializeField]
    private List<string> damageSources;

    public abstract bool IsDead { get; }

    public bool Attack { get; set; }

    //public int AttackDamage
    //{
    //    get
    //    {
    //        return 10;
    //    }
    //}

    public bool TakingDamage { get; set; }

    public Animator MyAnimator { get; set; }



    // Use this for initialization
    public virtual void Start()
    {
        health.Initialize();
        facingRight = true;
        MyAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public abstract IEnumerator TakeDamge(int damage);

    public abstract void Death();// new

    public virtual void ChangeDirection()
    {
        facingRight = !facingRight;
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        //if (damageSources.Contains(other.gameObject.tag))
        //{
        //    StartCoroutine(TakeDamge());
        //}
    }
}
